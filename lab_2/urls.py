from django.urls import path
from .views import index, xml, json

urlpatterns = [
    path('', index, name='index'), # admin
    # address - method views itu apa yang dipanggil
    path('xml', xml, name='xml'),
    path('json', json, name='json')
]

