from django import forms
from django.db import models


# Create your models here.


from .models import Friend


class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'

        error_messages = {
            'required': 'Please Type'
        }
        input_attrs = {
            'type': 'text',
            'placeholder': 'Your Name'
        }
        display_name = forms.CharField()


