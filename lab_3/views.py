from django.shortcuts import render
from django.shortcuts import HttpResponseRedirect
from .models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm(request.POST or None, request.FILES or None)
    if form.is_valid() and request.method == 'POST':
        form.save()
    context = {'form': form}
    return render(request, "lab3_form.html", context)
