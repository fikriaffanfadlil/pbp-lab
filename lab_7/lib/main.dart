import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Add Notes",
    theme: ThemeData(
      primarySwatch: Colors.indigo,
    ),
    home: const MyForm(),
  ));
}

class MyForm extends StatefulWidget {
  const MyForm({Key? key}) : super(key: key);

  @override
  _MyFormState createState() => _MyFormState();
}

class _MyFormState extends State<MyForm> {
  final _formKey = GlobalKey<FormState>();
  String? _penulis = " ";
  String? _matkul = " ";
  String? _topik = " ";
  String? _deskripsi = " ";
  String? _url = " ";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add Notes"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: "Contoh: Mark Zuckerberg",
                      labelText: "Penulis",
                      icon: const Icon(Icons.person),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        setState(() => _penulis = 'Kosong');
                        return 'Nama penulis tidak boleh kosong';
                      }
                      setState(() => _penulis = value);
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: "Contoh: Struktur Data dan Algoritma",
                      labelText: "Mata Kuliah",
                      icon: const Icon(Icons.book),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        setState(() => _matkul = 'Kosong');
                        return 'Mata kuliah tidak boleh kosong';
                      }
                      setState(() => _matkul = value);
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: "Contoh: Binary Tree",
                      labelText: "Topik",
                      icon: const Icon(Icons.topic),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        setState(() => _topik = 'Kosong');
                        return 'Topik tidak boleh kosong';
                      }
                      setState(() => _topik = value);
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: "Contoh: Catatan ini masih belum lengkap",
                      labelText: "Deskripsi",
                      icon: const Icon(Icons.description),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        setState(() => _deskripsi = 'Kosong');
                        return 'Catatan tidak boleh kosong';
                      }
                      setState(() => _deskripsi = value);
                      return null;
                    },
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: "Contoh: https://docs.google.com/...",
                      labelText: "URL Notes",
                      icon: const Icon(Icons.link),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        setState(() => _url = 'Kosong');
                        return 'URL tidak boleh kosong';
                      }
                      setState(() => _url = value);
                      return null;
                    },
                  ),
                ),
                ElevatedButton(
                  child: const Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                    }
                  },
                ),
                Text(
                  "\nNama Penulis yang disubmit: \n$_penulis\n",
                  style: const TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  "Mata kuliah yang disubmit: \n$_matkul\n",
                  style: const TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  "Topik yang disubmit: \n$_topik\n",
                  style: const TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  "Deskripsi yang disubmit: \n$_deskripsi\n",
                  style: const TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  "URL yang disubmit: \n$_url\n",
                  style: const TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
