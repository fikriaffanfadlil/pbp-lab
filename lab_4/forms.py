from django import forms
from django.db import models

# Create your models here.
from .models import Note


class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'

        error_messages = {
            'required': 'Please Type'
        }
        input_attrs = {
            'type': 'text',
            'placeholder': 'Your Name'
        }
        display_name = forms.CharField()
