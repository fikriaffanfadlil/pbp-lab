from django.shortcuts import render

from .forms import NoteForm
from .models import Note

from django.http.response import HttpResponse
from django.core import serializers


# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)


def add_note(request):

    form = NoteForm(request.POST or None, request.FILES or None)
    if form.is_valid() and request.method == 'POST':
        form.save()

    context = {'form': form}
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)
