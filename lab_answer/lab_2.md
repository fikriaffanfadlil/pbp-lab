1. Apakah perbedaan antara JSON dan XML?
JSON lebih unggul pada proyek yang memerlukan pertukaran data yang intensif dan lebih terorganisir. Pasalnya, JSON lebih cepat dan mudah dalam pemenuhan strukturisasi data dan mekanisme pertukaran. Sedangkan XML XML cocok diimplementasikan pada proyek yang memerlukan dokumen markup dan informasi metadata.
Selain itu, JSON dan XML memiliki berbagai perbedaan:
- JSON merupakan JavaScript Object Notation, sedangkan XML Extensible markup language
- JSON mendukung array, sedangkan XML tidak
- JSON mendukung komentar, sedangkan XML tidak
- JSON merupakan salah satu cara merepresentasikan objek, sedangkan XML adalah bahasa markup dan menggunakan tag structure untuk merepresentasi item data


2. Apakah perbedaan antara HTML dan XML?
HTML itu sendiri merupakan bahasa markup, sedangkan XML menyediakan framework untuk pendefinisian bahasa markup.
HTML digunakan untuk menampilkan dana dan fokus pada bagaimana data diperlihatkan. Sedangkan, XML digunakan untuk mentransportasikan dan menyimpan data. XML fokus pada apa data tersebut.
HTML itu statis karena digunakan untuk menampilkan data, sedangkan XML dinamis karena digunakan untuk transportasi data.